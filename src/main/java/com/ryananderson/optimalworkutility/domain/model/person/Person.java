package com.ryananderson.optimalworkutility.domain.model.person;

public class Person {
    private int _age;

    private String _firstName;
    private String _lastName;

    private Gender _gender;

    public Person() {
        this("firstname", "lastname", Gender.OTHER, 0);
    }

    public Person(String firstName, String lastName, Gender gender, int age) {
        _age = age;
        _firstName = firstName;
        _lastName = lastName;
        _gender = gender;
    }

    public int getAge() {
        return _age;
    }

    public void setAge(int age) {
        _age = age;
    }

    public String getFirstName() {
        return _firstName;
    }

    public void setFirstName(String firstName) {
        _firstName = firstName;
    }

    public String getLastName() {
        return _lastName;
    }

    public void setLastName(String lastName) {
        _lastName = lastName;
    }

    public Gender getGender() {
        return _gender;
    }

    public void setGender(Gender gender) {
        _gender = gender;
    }

    public String toString() {
        return String.format("Name: %s %s, Gender: %s, Age: %d", _firstName, _lastName, _gender.name().charAt(0) +
                _gender.name().substring(1).toLowerCase(), _age);
    }
}
