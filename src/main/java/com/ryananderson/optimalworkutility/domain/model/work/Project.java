package com.ryananderson.optimalworkutility.domain.model.work;

import org.springframework.format.annotation.DateTimeFormat;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Project {
    private double _budget;
    private double _hours;
    private double _overtimeHours;

    @DateTimeFormat(pattern = "yyyy-MM-dd'T'HH:mm")
    private Date _endDate;
    @DateTimeFormat(pattern = "yyyy-MM-dd'T'HH:mm")
    private Date _startDate;

    private String _projectName;

    private List<Worker> _workers;

    public Project() {
        this("", 0, 0, 0, new Date(), new Date(), new ArrayList<>());
    }

    public Project(String projectName, double budget, double hours, double overtimeHours, Date endDate, Date startDate, List<Worker> workers) {
        _projectName = projectName;
        _budget = budget;
        _hours = hours;
        _overtimeHours = overtimeHours;
        _endDate = endDate;
        _startDate = startDate;
        _workers = workers;
    }

    public void setBudget(double budget) {
        _budget = budget;
    }

    public double getBudget() {
        return _budget;
    }

    public void setHours(double hours) {
        _hours = hours;
    }

    public double getHours() {
        return _hours;
    }

    public void setOvertimeHours(double overtimeHours) {
        _overtimeHours = overtimeHours;
    }

    public double getOvertimeHours() {
        return _overtimeHours;
    }

    public void setEndDate(Date endDate) {
        _endDate = endDate;
    }

    public Date getEndDate() {
        return _endDate;
    }

    public void setStartDate(Date startDate) {
        _startDate = startDate;
    }

    public Date getStartDate() {
        return _startDate;
    }

    public void setProjectName(String projectName) {
        _projectName = projectName;
    }

    public String getProjectName() {
        return _projectName;
    }

    public List<Worker> getWorkers() {
        return _workers;
    }

    public void setWorkers(List<Worker> workers) {
        _workers = workers;
    }

    public String toString() {
        return String.format("Project Name: %s, Budget: %.2f, Hours: %f, Overtime Hours: %f, Start Date: %s, End Date: %s",
                _projectName,
                _budget,
                _hours,
                _overtimeHours,
                _startDate.toString(),
                _endDate.toString());
    }
}
