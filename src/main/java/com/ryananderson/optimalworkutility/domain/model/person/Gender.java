package com.ryananderson.optimalworkutility.domain.model.person;

public enum Gender {
    MALE("M"),
    FEMALE("F"),
    OTHER("O");

    private String _description;

    Gender(String description) {
        _description = description;
    }

    public String getDescription() {
        return _description;
    }
}
