package com.ryananderson.optimalworkutility.domain.model.work;

import com.ryananderson.optimalworkutility.domain.model.person.Gender;
import com.ryananderson.optimalworkutility.domain.model.person.Person;

public strictfp class Worker extends Person {
    private int _employeeIdentifier;

    private double _wage;

    private String _jobTitle;

    public static final double OVERTIME_WAGE_MULTIPLIER = 1.5;

    public Worker() {
        super();

        _employeeIdentifier = 0;
        _wage = 0;
        _jobTitle = "";
    }

    public Worker(String firstName, String lastName, Gender gender, int age, int employeeIdentifier, double wage, String jobTitle) {
        super(firstName, lastName, gender, age);

        _employeeIdentifier = employeeIdentifier;
        _wage = wage;
        _jobTitle = jobTitle;
    }

    public int getEmployeeIdentifier() {
        return _employeeIdentifier;
    }

    public void setEmployeeIdentifier(int employeeIdentifier) {
        _employeeIdentifier = employeeIdentifier;
    }

    public double getWage() {
        return _wage;
    }

    public void setWage(double wage) {
        _wage = wage;
    }

    public String getJobTitle() {
        return _jobTitle;
    }

    public void setJobTitle(String jobTitle) {
        _jobTitle = jobTitle;
    }

    public boolean equals(Worker worker) {
        return worker != null && worker.getEmployeeIdentifier() == _employeeIdentifier;
    }

    public String toString() {
        return String.format("%s, Employee Identifier: %d, Wage: %.2f, Job Title: %s", super.toString(), _employeeIdentifier, _wage, _jobTitle);
    }
}
