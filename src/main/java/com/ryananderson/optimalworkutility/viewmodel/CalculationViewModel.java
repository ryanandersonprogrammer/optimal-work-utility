package com.ryananderson.optimalworkutility.viewmodel;

import com.ryananderson.optimalworkutility.domain.model.work.Project;
import com.ryananderson.optimalworkutility.utility.OptimalWorkCalculatorUtility;

import java.time.Duration;

public class CalculationViewModel {
    private final double _pay;
    private final double _totalAllottedHours;
    
    private final long _totalAvailableHours;

    private Project _project;

    public CalculationViewModel(Project project) {
        _project = project;

        _pay = OptimalWorkCalculatorUtility.calculateOptimalWorkPay(_project);

        _totalAllottedHours = _project.getHours() + _project.getOvertimeHours();
        _totalAvailableHours = Duration
                .between(_project.getStartDate().toInstant(), _project.getEndDate().toInstant())
                .toHours();
    }

    public boolean getIsExcessivePay() {
        return _pay > _project.getBudget();
    }

    public Project getProject() {
        return _project;
    }

    public String getFormattedPay() {
        return String.format("%.2f USD", _pay);
    }

    public String getFormattedBudget() {
        return String.format("%.2f USD", _project.getBudget());
    }

    public String getFormattedTotalAllottedHours() {
        return String.format("%.4f", _project.getHours() + _project.getOvertimeHours());
    }

    public String getFormattedTotalAvailableHours() {
        return String.format("%d", _totalAvailableHours);
    }

    public String getFormattedMoneyRatio() {
        return String.format("%.8f", _pay / _project.getBudget());
    }

    public String getFormattedHourRatio() {
        return String.format("%.8f", _totalAllottedHours / _totalAvailableHours);
    }
}
