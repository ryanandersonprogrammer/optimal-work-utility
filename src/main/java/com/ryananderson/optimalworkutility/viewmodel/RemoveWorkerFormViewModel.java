package com.ryananderson.optimalworkutility.viewmodel;

import com.ryananderson.optimalworkutility.domain.model.work.Worker;

public class RemoveWorkerFormViewModel {
    private Worker _worker;

    public RemoveWorkerFormViewModel() {
        _worker = new Worker();
    }

    public Worker getWorker() {
        return _worker;
    }

    public void setWorker(Worker worker) {
        _worker = worker;
    }
}
