package com.ryananderson.optimalworkutility.viewmodel;

import com.ryananderson.optimalworkutility.domain.model.person.Gender;
import com.ryananderson.optimalworkutility.domain.model.work.Worker;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class AddWorkerFormViewModel {
    private Worker _worker;

    private List<Gender> _genders;

    public AddWorkerFormViewModel() {
        _worker = new Worker();

        _worker.setAge(14);
        _worker.setWage(7.25);
        _worker.setGender(Gender.MALE);

        _genders = Arrays.stream(Gender.values()).collect(Collectors.toList());
    }

    public Worker getWorker() {
        return _worker;
    }

    public void setWorker(Worker worker) {
        _worker = worker;
    }

    public List<Gender> getGenders() {
        return _genders;
    }

    public void setGenders(List<Gender> genders) {
        _genders = genders;
    }
}
