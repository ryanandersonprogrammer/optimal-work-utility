package com.ryananderson.optimalworkutility.viewmodel;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class OperationFormViewModel {
    private Operation _operation;
    private List<Operation> _operations;

    public OperationFormViewModel() {
        _operations = Arrays.stream(Operation.values()).filter(x -> x != Operation.NONE).collect(Collectors.toList());
    }

    public Operation getOperation() {
        return _operation;
    }

    public void setOperation(Operation operation) {
        _operation = operation;
    }

    public List<Operation> getOperations() {
        return _operations;
    }

    public void setOperations(List<Operation> operations) {
        _operations = operations;
    }

    public enum Operation {
        ADD("Add"), CREATE("Create"), LIST("List"), REMOVE("Remove"), RESTART("Restart"), NONE("None");

        private String _description;

        Operation(String description) {
            _description = description;
        }

        public String getDescription() {
            return _description;
        }
    }
}
