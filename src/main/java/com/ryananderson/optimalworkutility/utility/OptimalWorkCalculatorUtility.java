package com.ryananderson.optimalworkutility.utility;

import com.ryananderson.optimalworkutility.domain.model.work.Project;
import com.ryananderson.optimalworkutility.domain.model.work.Worker;

import java.util.List;

public class OptimalWorkCalculatorUtility {
    public static double calculateOptimalWorkPay(Project project) {
        List<Worker> workers;

        double sum;

        workers = project.getWorkers();

        sum = 0;

        for (Worker w : workers)
            sum += w.getWage() * (project.getHours() + Worker.OVERTIME_WAGE_MULTIPLIER * project.getOvertimeHours());

        return sum;
    }
}
