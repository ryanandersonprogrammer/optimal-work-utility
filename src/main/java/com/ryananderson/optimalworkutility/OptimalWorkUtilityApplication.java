package com.ryananderson.optimalworkutility;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OptimalWorkUtilityApplication {

    public static void main(String[] args) {
        SpringApplication.run(OptimalWorkUtilityApplication.class, args);
    }
}
