package com.ryananderson.optimalworkutility.controller;

import com.ryananderson.optimalworkutility.domain.model.work.Project;
import com.ryananderson.optimalworkutility.domain.model.work.Worker;
import com.ryananderson.optimalworkutility.viewmodel.*;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Controller
class OptimalWorkCalculatorController {
    private static final String REDIRECT_PREFIX = "redirect:/";
    private static final String REDIRECT_VIEW_NAME_CREATE = REDIRECT_PREFIX + "createProject";
    private static final String REDIRECT_VIEW_NAME_LIST = REDIRECT_PREFIX + "listWorkers";

    private Project _project;

    public OptimalWorkCalculatorController() {
        resetProject();
    }

    @ModelAttribute
    public void addAttributes(Model model) {
        model.addAttribute("project", _project);
    }

    @RequestMapping("")
    public String index(Model model) {
        model.addAttribute("operationFormViewModel", new OperationFormViewModel());

        return "index";
    }

    @PostMapping("/operate")
    public String processOperationForm(@ModelAttribute OperationFormViewModel operationFormViewModel) {
        switch (operationFormViewModel.getOperation()) {
            case ADD:
                return _project.getProjectName().isEmpty() ? REDIRECT_VIEW_NAME_CREATE : REDIRECT_PREFIX + "addWorker";
            case CREATE:
                return REDIRECT_VIEW_NAME_CREATE;
            case LIST:
                return REDIRECT_VIEW_NAME_LIST;
            case REMOVE:
                return _project.getProjectName().isEmpty() ? REDIRECT_VIEW_NAME_CREATE : REDIRECT_PREFIX + "removeWorker";
            case RESTART: {
                resetProject();

                return REDIRECT_PREFIX;
            }
            default:
                return "error";
        }
    }

    @GetMapping("/createProject")
    public String createProject() {
        return "createProject";
    }

    @RequestMapping("/addWorker")
    public String addWorker(@ModelAttribute Project project, Model model) {
        AddWorkerFormViewModel addWorkerFormViewModel;

        addWorkerFormViewModel = new AddWorkerFormViewModel();

        addWorkerFormViewModel.getWorker().setEmployeeIdentifier(_project.getWorkers().size() + 1);

        model.addAttribute("addWorkerFormViewModel", addWorkerFormViewModel);

        return "addWorker";
    }

    @PostMapping("/add")
    public String add(@ModelAttribute AddWorkerFormViewModel addWorkerFormViewModel) {
        List<Worker> workers;

        workers = _project.getWorkers();

        if (workers.stream().noneMatch(x -> x.equals(addWorkerFormViewModel.getWorker())))
            workers.add(addWorkerFormViewModel.getWorker());

        return REDIRECT_VIEW_NAME_LIST;
    }

    @RequestMapping("/removeWorker")
    public String removeWorker(@ModelAttribute Project project, Model model) {
        model.addAttribute("removeWorkerFormViewModel", new RemoveWorkerFormViewModel());

        return "removeWorker";
    }

    @PostMapping("/remove")
    public String remove(@ModelAttribute RemoveWorkerFormViewModel removeWorkerFormViewModel) {
        _project.getWorkers()
                .stream()
                .filter(x -> x.equals(removeWorkerFormViewModel.getWorker()))
                .findFirst()
                .ifPresent(x -> _project.getWorkers().remove(x));

        return REDIRECT_VIEW_NAME_LIST;
    }

    @RequestMapping(value = "/listWorkers", method = {RequestMethod.GET, RequestMethod.POST})
    public String listWorkers() {
        return "listWorkers";
    }

    @PostMapping("/calculate")
    public String calculate(Model model) {
        model.addAttribute("calculationViewModel", new CalculationViewModel(_project));

        return "calculation";
    }

    @InitBinder
    public void initBinder(WebDataBinder webDataBinder) {
        webDataBinder.registerCustomEditor(Date.class, new CustomDateEditor(new SimpleDateFormat("yyyy-MM-dd'T'HH:mm"), true));
    }

    private void resetProject() {
        _project = new Project();

        _project.setHours(1);
    }
}
