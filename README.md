﻿# Optimal Work Utility

## About



### Author

Ryan E. Anderson

---

### Description

This utility can be used to enter parameters and analyze an ordinal relation and ratio between potential work pay and a budget. The calculation is based on hours that were allotted at the beginning of a project.

---

### Version

1.0.0

---

### License

MIT